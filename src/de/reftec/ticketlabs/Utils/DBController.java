package de.reftec.ticketlabs.Utils;

import de.reftec.ticketlabs.Types.Event;
import de.reftec.ticketlabs.Types.Ticket;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DBController { 
	public static final int UPDATE_ERROR = 0;
	public static final int UPDATE_OK = 1;
	public static final int UPDATE_ID_NOT_AVAILABLE = 2;
	public static final int UPDATE_IS_USED = 3;
	public static final int SET_SYNC_OK = 4;
	public static final int SET_SYNC_ERROR = 5;
	
    private static final DBController dbcontroller = new DBController(); 
    private static Connection connection; 
//    private static final String DB_PATH = System.getProperty("user.home") + "/" + "ticketlabs.db"; 
//    private static final URI DB_PATH = Main.class.getResource("/ticketlabs.db").toURI();
//    private static final String DB_PATH = "/ticketlabs.db";
    
    static { 
        try { 
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) { 
            System.err.println("Fehler beim Laden des JDBC-Treibers"); 
            e.printStackTrace(); 
        } 
    }
    
    private DBController(){ 
    } 
     
    public static DBController getInstance(){ 
        return dbcontroller; 
    } 
     
    public void initDBConnection() { 
        try { 
            if (connection != null) 
                return; 
            System.out.println("Creating Connection to Database..."); 
//            connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH); 
            connection = DriverManager.getConnection("jdbc:sqlite:ticketlabs.db");
            if (!connection.isClosed()) 
                System.out.println("...Connection established"); 
        } catch (SQLException e) { 
            throw new RuntimeException(e); 
        } 

        Runtime.getRuntime().addShutdownHook(new Thread() { 
            public void run() { 
                try { 
                    if (!connection.isClosed() && connection != null) { 
                        clearEvents();
                        clearTickets();
                        connection.close();
                        if (connection.isClosed()) 
                            System.out.println("Connection to Database closed"); 
                    } 
                } catch (SQLException e) { 
                    e.printStackTrace(); 
                } 
            } 
        }); 
    } 
    
    public void handleDB() { 
        try { 
            Statement stmt = connection.createStatement();
            connection.setAutoCommit(true);
            stmt.executeUpdate("DROP TABLE IF EXISTS tickets;");
            stmt.executeUpdate("CREATE TABLE tickets (id integer, value double, event_id integer, is_used varchar, used varchar, synced varchar);"); 

            stmt.executeUpdate("DROP TABLE IF EXISTS events;");
            stmt.executeUpdate("CREATE TABLE events (id integer, title varchar, begin varchar, user_id integer);"); 
        } catch (SQLException e) { 
            System.err.println("Couldn't handle DB-Query"); 
	        Log.wirteLog("DBController-Create", e.getMessage());
            e.printStackTrace(); 
        } 
    }
    
    public void putTicket(Ticket ticket) {
    	try {
	        Statement stmt = connection.createStatement(); 
	        stmt.execute("INSERT INTO tickets (id, value, event_id, is_used, used, synced) VALUES " +
	        		"('" + ticket.getId() + "'," +
	        		"'" + ticket.getValue() + "',"+
	        		"'" + ticket.getEventId() + "'," +
	        		"'" + ticket.getIsUsed() + "'," +
	        		"'" + ticket.getDate() + " " + ticket.getTime() + "'," +
	        		"'false'" +
	        		")"); 
	    } catch (SQLException e) {
	        System.err.println("Couldn't handle DB-Query"); 
	        Log.wirteLog("DBController-PutTicket", e.getMessage());
	        e.printStackTrace(); 
	    } 
    }
    
    public void putEvent(Event event) {
    	try {
	        Statement stmt = connection.createStatement(); 
	        stmt.execute("INSERT INTO events (id, title, begin) VALUES " +
	        		"('" + event.getId() + "'," +
	        		"'" + event.getTitle() + "',"+
	        		"'" + event.getDate() + " " + event.getTime() + "'" +
	        		")"); 
	    } catch (SQLException e) { 
	        System.err.println("Couldn't handle DB-Query"); 
	        Log.wirteLog("DBController-PutTicket", e.getMessage());
	        e.printStackTrace(); 
	    } 
    }

    public ArrayList<Event> getEvents(String where) {
    	ArrayList<Event> events = new ArrayList<Event>();
    	try {
	        Statement stmt = connection.createStatement();
	        ResultSet rs;
	        if (where == null) rs = stmt.executeQuery("SELECT * FROM events;");
	        else rs = stmt.executeQuery("SELECT * FROM events WHERE " + where + ";");
	    	if (rs == null) return null;
	    	while (rs.next()) {
	        	Event tmp_event = new Event();
	        	tmp_event.setId(rs.getInt("id"));
	        	tmp_event.setTitle(rs.getString("title"));
	        	tmp_event.setDate(rs.getString("begin"));
	    		events.add(tmp_event);
	    	}
	    	rs.close();
	    } catch (SQLException e) { 
	        System.err.println("Couldn't handle DB-Query");
	        Log.wirteLog("DBController-getTickets", e.getMessage());
	        e.printStackTrace(); 
	    }
    	return events;
    }

    public ArrayList<Ticket> getTickets(String where) {
    	ArrayList<Ticket> tickets = new ArrayList<Ticket>();
    	try {
	        Statement stmt = connection.createStatement();
	        ResultSet rs;
	        if (where == null) rs = stmt.executeQuery("SELECT * FROM tickets;");
	        else rs = stmt.executeQuery("SELECT * FROM tickets WHERE " + where + ";");
	    	if (rs == null) return null;
	    	while (rs.next()) {
	        	Ticket tmp_ticket = new Ticket();
	    		tmp_ticket.setId(rs.getInt("id"));
	    		tmp_ticket.setValue(rs.getDouble("value"));
	    		tmp_ticket.setEventId(rs.getInt("event_id"));
	    		tmp_ticket.setIsUsed(Boolean.valueOf(rs.getString("is_used")));
	    		tmp_ticket.setUsed(rs.getString("used"));
	    		tickets.add(tmp_ticket);
	    	}
	    	rs.close();
	    } catch (SQLException e) { 
	        System.err.println("Couldn't handle DB-Query");
	        Log.wirteLog("DBController-getTickets", e.getMessage());
	        e.printStackTrace(); 
	    }
    	return tickets;
    }
    
    public Ticket getTicket(Long value) {
    	Ticket tmp_ticket = new Ticket();
    	try {
	        Statement stmt = connection.createStatement();
	    	ResultSet rs = stmt.executeQuery("SELECT * FROM tickets WHERE value = " + value + ";");
	    	if (rs == null) return null;
	    	while (rs.next()) {
	    		tmp_ticket.setId(rs.getInt("id"));
	    		tmp_ticket.setValue(rs.getDouble("value"));
	    		tmp_ticket.setEventId(rs.getInt("event_id"));
	    		tmp_ticket.setIsUsed(Boolean.valueOf(rs.getString("is_used")));
	    		tmp_ticket.setUsed(rs.getString("used"));
//		    	System.out.println(tmp_ticket.toString());
	    	}
	    	rs.close();
	    } catch (SQLException e) { 
	        System.err.println("Couldn't handle DB-Query");
	        Log.wirteLog("DBController-getTicket", e.getMessage());
	        e.printStackTrace(); 
	    }
    	return tmp_ticket;
    }
    
    public int updateTicket(Ticket ticket) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	try {
			if (ticket.getIsUsed())
				return UPDATE_IS_USED;
			else {
				Date today = Calendar.getInstance().getTime();
		        Statement stmt = connection.createStatement();
		        String sql = "UPDATE tickets SET " +
		        		"used = '" + dateFormat.format(today) + "', " +
		        		"is_used = 'true' " +
		        		"WHERE value = '" + ticket.getValue() + "'";
//		        System.out.println(sql);
		        int result = stmt.executeUpdate(sql);
		        if (result == 1) return UPDATE_OK;
			}
	    } catch (SQLException e) { 
	        System.err.println("Couldn't handle DB-Query"); 
	        Log.wirteLog("DBController-UpdateTicket", e.getMessage());
	        e.printStackTrace(); 
	    }
    	return UPDATE_ERROR;
    }
    
    public int syncTicket(Ticket ticket) {
    	try {
	        Statement stmt = connection.createStatement();
	        String sql = "UPDATE tickets SET synced = 'true' " +
	        		"WHERE value = '" + ticket.getValue() + "'";
	        int result = stmt.executeUpdate(sql);
	        if (result == 1) return SET_SYNC_OK;
	    } catch (SQLException e) { 
	        System.err.println("Couldn't handle DB-Query"); 
	        Log.wirteLog("DBController-SetSync", e.getMessage());
	        e.printStackTrace(); 
	    }
    	return SET_SYNC_ERROR;
    }
    
    public void clearTickets() {
    	try {
	    	Statement stmt = connection.createStatement();
	    	stmt.execute("DELETE FROM tickets");
	    } catch (SQLException e) { 
	        System.err.println("Couldn't handle DB-Query"); 
	        Log.wirteLog("DBController-Clear", e.getMessage());
	        e.printStackTrace(); 
	    } 
    }
    
    public void clearEvents() {
    	try {
	    	Statement stmt = connection.createStatement();
	    	stmt.execute("DELETE FROM events");
	    } catch (SQLException e) { 
	        System.err.println("Couldn't handle DB-Query"); 
	        Log.wirteLog("DBController-Clear", e.getMessage());
	        e.printStackTrace(); 
	    } 
    }
}