package de.reftec.ticketlabs.Utils;

import com.eclipsesource.json.JsonObject;

import de.reftec.ticketlabs.Main.RestAPI;

public class JSONutil {
	
	protected RestAPI reference;
	protected JsonObject outer_obj;
	protected JsonObject filter_obj;
	
	/**
	 * <b>IMPORTANT</b></br>
	 * This method is mandatory for the class usage.</br>
	 * Will set the reference for your restAPI.
	 * @param reference
	 */
	public void setReference(RestAPI reference) {
		this.reference = reference;
		if (outer_obj == null) outer_obj = new JsonObject();
	}
	
	/**
	 * @param key
	 * @param value
	 * @return reference
	 */
	public RestAPI setFilter(String key, Object value)
	{
		if (filter_obj == null) filter_obj = new JsonObject();
		
		if (value instanceof Integer) filter_obj.add(key, (Integer)value);
		else if (value instanceof Double) filter_obj.add(key, (Double)value);
		else if (value instanceof Float) filter_obj.add(key, (Float)value);
		else if (value instanceof Long) filter_obj.add(key, (Long)value);
		else if (value instanceof Boolean) filter_obj.add(key, (Boolean)value);
		else if (value instanceof String) filter_obj.add(key, (String)value);
		else return reference;
		
		outer_obj.add("filter", filter_obj);
		return reference;
	}
	
	/**
	 * Set the <b>limit</b> for the results per page
	 * @param limit
	 * @return reference
	 */
	public RestAPI setLimit(int limit)
	{
		outer_obj.add("limit", limit);
		return reference;
	}
	
	/**
	 * Set the <b>page</b> for the results </br>
	 * (limit offset)
	 * @param page
	 * @return reference
	 */
	public RestAPI setPage(int page)
	{
		outer_obj.add("page", page);
		return reference;
	}
	
	/**
	 * @return generated JSON string
	 */
	protected String getResultString() {
		String result = outer_obj.toString();
		System.out.println("Filter: " + result);
		return result;
	}
	
	/**
	 * Clear the filter for the next use.
	 */
	protected void clearFilter() {
		filter_obj = null;
		outer_obj = new JsonObject();
	}
}
