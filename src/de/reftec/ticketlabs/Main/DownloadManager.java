package de.reftec.ticketlabs.Main;

import de.reftec.ticketlabs.Types.Event;
import de.reftec.ticketlabs.Utils.DBController;

public class DownloadManager {
    
    private DownloadManager(){ 
    }
    
    public static void start (RestAPI rest) {
    	rest.setFilter("is_active", true).getEvents();
    	for (Event event : DBController.getInstance().getEvents(null)) {
    		DBController.getInstance().putEvent(event);
    		rest.setFilter("event_id", event.getId()).getTickets();
    	}
    }
}