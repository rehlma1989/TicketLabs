package de.reftec.ticketlabs.Main;

import de.reftec.ticketlabs.Types.Event;
import de.reftec.ticketlabs.Types.Ticket;
import de.reftec.ticketlabs.Utils.DBController;
import de.reftec.ticketlabs.Utils.Log;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.SortType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class Main extends Application implements Initializable, ChangeListener<String>{
	@FXML private TextField textField_ScannedValue;
	@FXML private TextField textField_username;
	@FXML private PasswordField passwordField_pin;
	@FXML private AnchorPane anchorPane_Check;
	@FXML private AnchorPane anchorPane_titleBar;
	@FXML private AnchorPane anchorPane_stageContainer;
	@FXML private Label label_Check;
	@FXML private Label label_Event;
	@FXML private HBox contentContainer;
	@FXML private HBox loginContainer;
	@FXML private VBox loginInnerElement;
	@FXML private Button closeButton;
	@FXML private Button fullscreenButton;
	@FXML private Button button_login;
	@FXML private Pane pane_LogoHolder;
	@FXML private TableView<Ticket> tableView_tickets;
	@FXML private ComboBox<Event> comboBox_Events;
	@FXML private CheckBox checkBox_UsedTickets;
	@FXML private Group group_Logo;
	@FXML private Label label_login;
	@FXML private Label label_text;
	@FXML private Tooltip tooltip_CheckBox;
	
	private final int CHECK_OK = 1;
	private final int CHECK_WRONG_CODE = 2;
	private final int CHECK_WRONG_EAN = 3;
	private double xOffset;
	private double yOffset;
	private ObservableList<Event> observableList_events;
	private ObservableList<Ticket> observableList_tickets;
	private RestAPI rest;
	private Timer sync_timer = new Timer();
	private TimerTask upload_task;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(final Stage stage) throws Exception {
		Font.loadFont(getClass().getResource("/OpenSans-Regular.ttf").toURI().toString(), 14);
		Font.loadFont(getClass().getResource("/OpenSans-Semibold.ttf").toURI().toString(), 14);
		Font.loadFont(getClass().getResource("/OpenSans-Light.ttf").toURI().toString(), 14);
		
		// Create DB
        DBController dbc = DBController.getInstance(); 
        dbc.initDBConnection(); 
        dbc.handleDB();
		
		// Language xml laden
		Resources.loadLanguage("german");
		
		// FXML-Datei laden!
		Parent root = FXMLLoader.load(getClass().getResource("/Layout.fxml"));
		Log.wirteLog("Layout", "Load: " + getClass().getResource("/Layout.fxml").toURI().toString());

		// Szene
		Scene scene = new Scene(root);
		stage.initStyle(StageStyle.UNDECORATED);
		
		// Szene setzen
		stage.setScene(scene);
		stage.sizeToScene();
		
		// Stage anzeigen
		stage.show();
	}

	// Initialisierung von Events und Handelern
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		/*++++++++++++++++++++++++SET TEXT+++++++++++++++++++++++++++++++++++++++++++*/
		textField_ScannedValue.setPromptText(Resources.getString("main_textField_ticketnumber"));
		tooltip_CheckBox.setText(Resources.getString("main_tooltip_checkBox_onlyUsed"));
		label_login.setText(Resources.getString("main_label_login"));
		label_text.setText(Resources.getString("main_label_loginText"));
		textField_username.setPromptText(Resources.getString("main_textField_username"));
		passwordField_pin.setPromptText(Resources.getString("main_passwordField_pin"));
		button_login.setText(Resources.getString("main_button_login"));
		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		
		rest = new RestAPI();
		
		textField_username.setText("application");
		passwordField_pin.setText("SafetyFirst!");
		
		contentContainer.getStyleClass().add("show");
		loginContainer.getStyleClass().add("show");

		// make the window draggable
		anchorPane_titleBar.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				// record a delta distance for the drag and drop operation.
				xOffset = anchorPane_titleBar.getScene().getWindow().getX() - mouseEvent.getScreenX();
				yOffset = anchorPane_titleBar.getScene().getWindow().getY() - mouseEvent.getScreenY();
				anchorPane_titleBar.getScene().setCursor(Cursor.MOVE);
			}
		});
		anchorPane_titleBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				anchorPane_titleBar.getScene().getWindow().setX(mouseEvent.getScreenX() + xOffset);
				anchorPane_titleBar.getScene().getWindow().setY(mouseEvent.getScreenY() + yOffset);
			}
		});
		
		// Timer for syncronize each 5 minutes
		upload_task = new TimerTask() {
			@Override
			public void run() {
				UploadManager.start(rest);
			}
		};
	}
	
	/**
	 * Change method for the scanned value
	 */
	@Override
	public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		String result = newValue;
		FadeTransition ft = new FadeTransition(Duration.millis(500), label_Check);
		
		if (!result.matches("[0-9]*") || result.length() > 13) {
			result = oldValue;
			textField_ScannedValue.setText(result);
		}
		
		if (result.length() == 1 || result.length() == 0) {
			textField_ScannedValue.getStyleClass().removeAll("warning", "error", "success", "default");
			textField_ScannedValue.getStyleClass().add("default");
			
			label_Check.setText("");
			// Fade-Out the check label
			ft.setToValue(0);
			
			anchorPane_Check.getStyleClass().removeAll("warning", "error", "success", "hidden");
			anchorPane_Check.getStyleClass().add("hidden");
		}
		else if (result.length() == 13) {
			// Select all numbers
			Platform.runLater(new Runnable() {
                @Override
                public void run() {
                	textField_ScannedValue.selectAll();
                }
            });
			
			// Fade-In the check label
			ft.setToValue(1);
			
			switch (checkValue(result)) {
			case CHECK_OK: {
				long value = Long.parseLong(result.substring(0, result.length()));
				Ticket ticket = DBController.getInstance().getTicket(value);
				if (ticket == null) {
					textField_ScannedValue.getStyleClass().removeAll("warning", "error", "success", "default");
					textField_ScannedValue.getStyleClass().add("error");
					
					anchorPane_Check.getStyleClass().removeAll("warning", "error", "success", "hidden");
					anchorPane_Check.getStyleClass().add("error");
					label_Check.setText(Resources.getString("main_error_ticketNotAvailable"));
					break;	
				}
				label_Event.setText(getEvent(ticket.getEventId()));
				
				int res = DBController.getInstance().updateTicket(ticket);
				switch (res) {
				case DBController.UPDATE_ERROR:
					textField_ScannedValue.getStyleClass().removeAll("warning", "error", "success", "default");
					textField_ScannedValue.getStyleClass().add("error");
					
					anchorPane_Check.getStyleClass().removeAll("warning", "error", "success", "hidden");
					anchorPane_Check.getStyleClass().add("error");
					label_Check.setText(Resources.getString("main_error"));
					break;
				case DBController.UPDATE_IS_USED:
					textField_ScannedValue.getStyleClass().removeAll("warning", "error", "success", "default");
					textField_ScannedValue.getStyleClass().add("error");
					
					anchorPane_Check.getStyleClass().removeAll("warning", "error", "success", "hidden");
					anchorPane_Check.getStyleClass().add("error");
					label_Check.setText(Resources.getString("main_error_ticketIsUsed") + " " + ticket.getTime());
					break;
				case DBController.UPDATE_OK:
					textField_ScannedValue.getStyleClass().removeAll("warning", "error", "success", "default");
					textField_ScannedValue.getStyleClass().add("success");
					
					anchorPane_Check.getStyleClass().removeAll("warning", "error", "success", "hidden");
					anchorPane_Check.getStyleClass().add("success");
					label_Check.setText(Resources.getString("main_ticketAccept"));
					updateTicket(DBController.getInstance().getTicket(value));

					// Refresh tableView by visibility
					tableView_tickets.getColumns().get(0).setVisible(false);
					tableView_tickets.getColumns().get(0).setVisible(true);
					break;
				}
				break;
			}
			case CHECK_WRONG_CODE:
				textField_ScannedValue.getStyleClass().removeAll("warning", "error", "success", "default");
				textField_ScannedValue.getStyleClass().add("warning");
				
				anchorPane_Check.getStyleClass().removeAll("warnig", "error", "success", "hidden");
				anchorPane_Check.getStyleClass().add("warning");
				label_Check.setText(Resources.getString("main_error_code"));
				break;
			case CHECK_WRONG_EAN:
				textField_ScannedValue.getStyleClass().removeAll("warning", "error", "success", "default");
				textField_ScannedValue.getStyleClass().add("warning");
				
				anchorPane_Check.getStyleClass().removeAll("warning", "error", "success", "hidden");
				anchorPane_Check.getStyleClass().add("warning");
				label_Check.setText(Resources.getString("main_error_ean"));
				break;
			}
		}
		ft.play();
	}
	
	@FXML
	private void closeButtonAction() {
		upload_task.cancel();
		sync_timer.cancel();

		Service<Void> service = new Service<Void>() {  
		    @Override  
		    protected Task<Void> createTask() {  
		    return new Task<Void>() {  
		            @Override  
		            protected Void call() throws Exception {
						UploadManager.start(rest);
		                return null;  
		            }  
		        };  
		    }  
		    @Override  
		    protected void succeeded() {  
				Stage stage = (Stage) closeButton.getScene().getWindow();
				stage.close();
		    }  
		};  
		service.start(); // starts Thread
	}

	@FXML
	private void fullscreenButtonAction() {
		Stage stage = (Stage) fullscreenButton.getScene().getWindow();
	     
		if (stage.isFullScreen()) {
			stage.setFullScreen(false);
			loginInnerElement.setPrefSize(700, 500);
			loginInnerElement.setMaxSize(700, 500);
			loginContainer.setAlignment(Pos.TOP_LEFT);
			anchorPane_stageContainer.getStyleClass().remove("wide");
		}
		else {
			stage.setFullScreen(true);
			loginInnerElement.setMaxSize(1280, 500);
			loginInnerElement.setPrefSize(1280, 500);
			loginContainer.setAlignment(Pos.CENTER);
			anchorPane_stageContainer.getStyleClass().add("wide");
		}
	}

	@FXML
	private void loginButtonAction() {
		if(rest.login(textField_username.getText(), passwordField_pin.getText()) == RestAPI.LOGIN_OK) {

			Service<Void> service = new Service<Void>() {  
			    @Override  
			    protected Task<Void> createTask() {  
			    return new Task<Void>() {  
			            @Override  
			            protected Void call() throws Exception {  
							DownloadManager.start(rest);
		                    return null;  
			            }  
			        };  
			    }  
			    @Override  
			    protected void succeeded() {  
			    	FadeTransition ft = new FadeTransition(Duration.millis(1500), loginContainer);
					ft.setToValue(0);
					ft.play();
					
					ft.setOnFinished(new EventHandler<ActionEvent>() {
				        @Override
				        public void handle(ActionEvent event) {
							loginContainer.getStyleClass().removeAll("show");
							contentContainer.getStyleClass().removeAll("hidden");
							loginContainer.getStyleClass().add("hidden");
							contentContainer.getStyleClass().add("show");
							textField_ScannedValue.requestFocus();
				        }
				    });
					myInit();
					sync_timer.schedule(upload_task, 300000, 300000); 
			    }  
			};  
			service.start(); // starts Thread
		}
	}

	private int checkValue(String value) {
		int checkSum_1 = 0;
		int checkSum_2 = 0;
		int ean_even = 0;
		int ean_uneven = 0;
		for (int i = 0; i < value.length(); i++) {
			// Calculate ours checksum
			if (i < 11)
				checkSum_1 += Integer.parseInt(value.substring(i, i + 1));
			if (i == 11)
				checkSum_1 = checkSum_1 % 9;
			// Calculate EAN checksum
			if (i < 12) {
				if ((i + 1) % 2 == 0)
					ean_even += Integer.parseInt(value.substring(i, i + 1));
				else
					ean_uneven += Integer.parseInt(value.substring(i, i + 1));
			}
			if (i == 12) {
				checkSum_2 = 10 - ((ean_even * 3 + ean_uneven) % 10);
				if (checkSum_2 == 10)
					checkSum_2 = 0;
			}
			
			// Check
			if (i == 12) {
				if (checkSum_1 != Integer.parseInt(value.substring(11, 12)))
					return CHECK_WRONG_CODE;
				if (checkSum_2 != Integer.parseInt(value.substring(12, 13)))
					return CHECK_WRONG_EAN;
			}
		}
		return CHECK_OK;
	}

	private String getEvent(int eventID) {
		String result = "";
		for (Event ev : observableList_events) {
			if (ev.getId() == eventID)
				result = ev.getTitle();
		}
		return result;
	}

	// set all options and load data
	private void myInit() {
		textField_ScannedValue.textProperty().addListener(this);
		
		observableList_events = FXCollections.observableArrayList(DBController.getInstance().getEvents(null));
		observableList_tickets = FXCollections.observableArrayList();
		
		checkBox_UsedTickets.setSelected(true);
		
		comboBox_Events.setItems(observableList_events);
		comboBox_Events.valueProperty().addListener(new ChangeListener<Event>() {
			@Override
			public void changed(ObservableValue<? extends Event> arg0, Event arg1, Event arg2) {
				observableList_tickets.clear();
				if(checkBox_UsedTickets.isSelected())
					observableList_tickets.setAll(DBController.getInstance().getTickets("is_used = 'true' and event_id = '" + arg2.getId() + "'"));
				else
					observableList_tickets.setAll(DBController.getInstance().getTickets("event_id = '" + arg2.getId() + "'"));
				
				ArrayList<TableColumn<Ticket, ?>> sortOrder = new ArrayList<TableColumn<Ticket, ?>>(tableView_tickets.getSortOrder());
				tableView_tickets.getSortOrder().clear();
				tableView_tickets.getSortOrder().addAll(sortOrder);
			}
		});
		comboBox_Events.getSelectionModel().select(0);
		checkBox_UsedTickets.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				observableList_tickets.clear();
				if (arg2)
					observableList_tickets.setAll(DBController.getInstance().getTickets("is_used = 'true' and event_id = '" + comboBox_Events.getSelectionModel().getSelectedItem().getId() + "'"));
				else
					observableList_tickets.setAll(DBController.getInstance().getTickets("event_id = '" + comboBox_Events.getSelectionModel().getSelectedItem().getId() + "'"));
				
				ArrayList<TableColumn<Ticket, ?>> sortOrder = new ArrayList<TableColumn<Ticket, ?>>(tableView_tickets.getSortOrder());
				tableView_tickets.getSortOrder().clear();
				tableView_tickets.getSortOrder().addAll(sortOrder);
			}
		});
		
		TableColumn<Ticket, Long> valueCol = new TableColumn<Ticket, Long>("Ticketnummer");
		valueCol.setMinWidth(150);
		valueCol.setCellValueFactory(new PropertyValueFactory<Ticket, Long>("value"));
		tableView_tickets.getColumns().add(valueCol);
		
		TableColumn<Ticket, String> dateCol = new TableColumn<Ticket, String>("Datum");
		dateCol.setMinWidth(150);
		dateCol.setCellValueFactory(new PropertyValueFactory<Ticket, String>("date"));
		dateCol.setSortType(SortType.DESCENDING);
		tableView_tickets.getColumns().add(dateCol);
		
		TableColumn<Ticket, String> timeCol = new TableColumn<Ticket, String>("Zeit");
		timeCol.setMinWidth(100);
		timeCol.setCellValueFactory(new PropertyValueFactory<Ticket, String>("time"));
		timeCol.setSortType(SortType.DESCENDING);
		tableView_tickets.getColumns().add(timeCol);
		
		tableView_tickets.setItems(observableList_tickets);
		
		tableView_tickets.getSortOrder().clear();
		tableView_tickets.getSortOrder().add(dateCol);
		tableView_tickets.getSortOrder().add(timeCol);
	}
	
	private void updateTicket(Ticket ticket) {
		boolean isInList = false;
		for (Ticket t : observableList_tickets) {
			if (t.getValue() == ticket.getValue()) {
				isInList = true;
				t.setIsUsed(ticket.getIsUsed());
				t.setUsed(ticket.getDate() + " " + ticket.getTime());
			}
		}
		if (!isInList) observableList_tickets.add(0, ticket);
	}
	
	@SuppressWarnings("unused")
	private void adjustTransform() {
		group_Logo.getTransforms().clear();
		double cx = group_Logo.getBoundsInParent().getMinX();
		double cy = group_Logo.getBoundsInParent().getMinY();
		double cw = group_Logo.getBoundsInParent().getWidth();
		double ch = group_Logo.getBoundsInParent().getHeight();
		double ew = pane_LogoHolder.getPrefWidth();
		double eh = pane_LogoHolder.getPrefHeight();
		if (ew > 0.0 && eh > 0.0) {
			double scale = Math.min(ew / cw, eh / ch);
			// Offset to center content
			double sx = 0.5 * (ew - cw * scale);
			double sy = 0.5 * (eh - ch * scale);
			group_Logo.getTransforms().add(new Translate(sx, sy));
			group_Logo.getTransforms().add(new Translate(-cx, -cy));
			group_Logo.getTransforms().add(new Scale(scale, scale, cx, cy));
		}
	}
}