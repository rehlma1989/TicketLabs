package de.reftec.ticketlabs.Main;

import java.util.ArrayList;

import de.reftec.ticketlabs.Types.Ticket;
import de.reftec.ticketlabs.Utils.DBController;

public class UploadManager {
    
    private UploadManager(){ 
    }
    
    public static void start (RestAPI rest) {
    	ArrayList<Ticket> sync = DBController.getInstance().getTickets("synced='false' and is_used='true'");
    	for (Ticket ticket : sync) {
    		int res = rest.updateTicket(ticket);
    		switch (res) {
    		case RestAPI.UPDATE_ERROR:
    			break;
    		case RestAPI.HTTP_ERROR:
    			break;
    		case RestAPI.UPDATE_OK:
    			DBController.getInstance().syncTicket(ticket);
    			break;
    		}
    	}
    }
}