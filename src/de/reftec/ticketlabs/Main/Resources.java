package de.reftec.ticketlabs.Main;

import java.io.File;
import java.net.URI;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.reftec.ticketlabs.Utils.Log;

public class Resources {
	private static HashMap<String, String> language_strings;
	
	/**
	 * load all strings for the language
	 * @param language
	 */
	public static void loadLanguage(String language) {
		language_strings = new HashMap<String, String>();
		try {
			URI file = Main.class.getResource("/language/"+language+".xml").toURI();
			Log.wirteLog("LanguageFile", "Load: " + file.toString());
			File fXmlFile = new File(file);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("string");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				 
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) nNode;
					language_strings.put(
							eElement.getAttribute("name"), 
							eElement.getTextContent()
							);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param name / key
	 * @return the value for this name / key
	 */
	public static String getString(String name) {
		if(language_strings.isEmpty() || language_strings == null) return "";
		return language_strings.get(name);
	}
}
